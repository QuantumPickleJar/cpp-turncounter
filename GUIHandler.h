#ifndef __GUIHANDLER_H__
#define __GUIHANDLER_H__

// #include "Player.h"
#include "RosterManager.h"

#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0xF800
#define GREEN 0x07E0
#define FORESTGREEN 0x0400
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF
#define GRAY 0xAD35
#define GRAY1 0xAD35
#define GRAY2 0x62EC
#define GRAY3 0x3186
#define GRAY4 0x6B0C
#define GRAY5 0x83D0
#define MINPRESSURE 200
#define MAXPRESSURE 1000

class GUIHandler
{
    RosterManager* rManager;
    static const uint16_t INIT_ROW_BTN_X = 290;
    static const uint16_t INIT_ROW_BTN_Y = 419;
    static const uint16_t ROW_BTN_HEIGHT = 30;
    static const uint16_t ROW_BTN_WIDTH = 30;
    static const int LEFT_MARGIN = 15;
    static const int XP = 8, XM = A2, YP = A3, YM = 9; //240x320 ID=0x9341
    static const int TS_LEFT = 62, TS_RT = 882, TS_TOP = 920, TS_BOT = 101;
    TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);
    int pixel_x, pixel_y; //Touch_getXY() updates global vars

  //  Adafruit_GFX_Button next_btn, reset_btn, start_btn, r0, r1, r2, r3, r4, r5, r6;
    // int* NUM_PLAYERS = &Roster::NUM_PLAYERS;
    // player_type players[Roster::NUM_PLAYERS];

public:
    player_type* players;

    //make empty buttons to be matched with byref buttons from main()
    Adafruit_GFX_Button next_btn, reset_btn, start_btn, r0, r1, r2, r3, r4, r5, r6;

    //constructor
    GUIHandler(
        RosterManager &manager,
        Adafruit_GFX_Button &start_btn,
        Adafruit_GFX_Button &reset_btn,
        Adafruit_GFX_Button &next_btn,
        Adafruit_GFX_Button &r0,
        Adafruit_GFX_Button &r1,
        Adafruit_GFX_Button &r2,
        Adafruit_GFX_Button &r3,
        Adafruit_GFX_Button &r4,
        Adafruit_GFX_Button &r5,
        Adafruit_GFX_Button &r6
    ){
        //set a singular instance of RosterManager
        this->rManager = &manager;
        this->start_btn = start_btn,
        this->reset_btn = reset_btn,
        this->next_btn = next_btn,
        this->r0 = r0,
        this->r1 = r1,
        this->r2 = r2,
        this->r3 = r3,
        this->r4 = r4,
        this->r5 = r5,
        this->r6 = r6;
//        players = Roster::players;
        this->players = rManager->GetPlayerArray();
    }   
//Adafruit_GFX_Button *buttons[] = {&r0, &r1, &r2, &r3, &r4, &r5, &r6, &reset_btn, &next_btn, &start_btn, NULL};   

     int GetX() { return pixel_x; }
     int GetY() { return pixel_y; }
    
    bool Touch_getXY(void)
    {
        TSPoint p = ts.getPoint();
        pinMode(YP, OUTPUT); //restore shared pins
        pinMode(XM, OUTPUT);
        digitalWrite(YP, HIGH); //because TFT control pins
        digitalWrite(XM, HIGH);
        bool pressed = (p.z > MINPRESSURE && p.z < MAXPRESSURE);
        if (pressed)
        {
            pixel_x = map(p.y, TS_LEFT, TS_RT, 0, tft.width()); //.kbv makes sense to me
            pixel_y = map(p.x, TS_TOP, TS_BOT, 0, tft.height());
        }
        Serial.println("x = ");
        Serial.print(pixel_x);
        
        Serial.println("y = ");
        Serial.print(pixel_y);

        return pressed;
    }

    void makeButtons()
    {
        start_btn.initButton(&tft, 65, 16, 75, 30, BLACK, GRAY3, WHITE, "BEGIN", 2);
        next_btn.initButton(&tft, 165, 16, 75, 30, BLACK, GRAY3, WHITE, "NEXT", 2);
        reset_btn.initButton(&tft, 265, 16, 75, 30, BLACK, GRAY3, WHITE, "NEW", 2);

        int button_index = 0;

        r0.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y, ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r1.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r2.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r3.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r4.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r5.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);
        button_index++;
        r6.initButtonUL(&tft, INIT_ROW_BTN_X, INIT_ROW_BTN_Y + (button_index * ROW_BTN_HEIGHT), ROW_BTN_WIDTH, ROW_BTN_HEIGHT, BLACK, GRAY3, WHITE, ">>", 2);

        r0.drawButton(false);
        r1.drawButton(false);
        r2.drawButton(false);
        r3.drawButton(false);

        r4.drawButton(false);
        r5.drawButton(false);
        r6.drawButton(false);
        start_btn.drawButton(false);
        reset_btn.drawButton(false);
        next_btn.drawButton(false);
    }

    void printPlayer(int i)
    {   
        if (rManager->GetIsSorted()) 
        {
            tft.print(players[i].name + "/" + players[i].player_name); // + "\t [" + players[i].turn_order+ "]");
            tft.setCursor((tft.getCursorX() + 40), tft.getCursorY());  //shift cursor
            tft.print(players[i].turn_order->Get());
        }
        else tft.print(players[i].name + "/" + players[i].player_name);
    }

    void printPlayerFixed(int i)
    {
        tft.setCursor(LEFT_MARGIN, (INIT_ROW_BTN_Y + (i * ROW_BTN_HEIGHT)));
        printPlayer(i);
    }

    //used for printing even if the order isn't set yet
    void printNames()
    {

        for (int i = 0; i <= Roster::NUM_PLAYERS; i++)
        {
            uint16_t temp = INIT_ROW_BTN_Y + (i * ROW_BTN_HEIGHT);
            tft.setCursor(LEFT_MARGIN, temp);

            //the final player is supposed to be the enemy, so print them in red
            if (i == Roster::NUM_PLAYERS) {
                tft.setTextColor(RED);
                printPlayer(i);
                tft.setTextColor(WHITE);
            }
            else {
                printPlayer(i);
            }
        }
    }

    void printAllButOne(int index)
    {
        for (int i = 0; i <= Roster::NUM_PLAYERS; i++)
        {
            if (i != index) {
                printPlayerFixed(i);
            }
        }
    }

    //prints one player in a given color, default color is FORESTGREEN
    void printPlayerColored(int index, uint16_t color = FORESTGREEN)
    {
        tft.setCursor(LEFT_MARGIN, (INIT_ROW_BTN_Y + (index * ROW_BTN_HEIGHT)));
        tft.setTextColor(color);
        // printPlayer(index, false);
        printPlayerFixed(index);
        tft.setTextColor(WHITE);
        //print every other player
        printAllButOne(index);
    }
};

#endif