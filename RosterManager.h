#ifndef __ROSTERMANAGER_H__
#define __ROSTERMANAGER_H__
#include "Roster.h"
//class that holds the player data
class RosterManager
{
    player_type *players; //think of this as a shortcut to a file--It just holds the path to the actual thing

public:
    bool isFirstSelected;
    const int m_NUM_PLAYERS = Roster::NUM_PLAYERS;
    //constructor
    RosterManager()
    {
        // players = Roster::players;
        Roster* r = new Roster;
        players = r->players;
        isFirstSelected = true;
        delete r;
    }

    void resetTurnOrder(){
        for (int i = 0; i < sizeof(players); i++)
        {
            if(players[i].turn_order) delete players[i].turn_order;
            // if(players[i].turn_order) players[i].turn_order == nullptr;
        }
        isFirstSelected = true;
    }

    bool AnyTurnSet(){
        player_type* p = GetPlayerArray();
        for (int i = 0; i < sizeof(p); i++)
        {
            if(players[i].turn_order != NULL) {
                if(p) delete p;         //swap line below to stop "delete on heap" based error
                // if(p) p == nullptr;
                return false;
            } 
        }
        if(p) delete p;         //swap line below to stop "delete on heap" based error
        // if(p) p == nullptr;
        return true;

    }

    void assignOrder(int index)
    {
        //TODO: need to add some way to handle accidentally setting the same item's order twice

        if (AnyTurnSet())
        {
            //since this is the first one to be selected, set this to be the first
            players[index].turn_order->Set(1);
            Serial.println(">> assignOrder() ln:262 :: detected as true");
            isFirstSelected = false;
        }
        else
        {
            //find lowest non-zero turn order
            int lowest = NUM_PLAYERS;
            for (int i = 0; i < NUM_PLAYERS; i++)
            {
                //if the turn order is lower
                if (players[i].turn_order->Get() < lowest)
                {
                        lowest = players[i].turn_order->Get();
                        players[index].turn_order->Set(lowest++);
                }
            }
            Serial.println("Lowest turn_order detected: ");
            Serial.print(lowest);
        }
    }
    bool GetIsSorted()
    {
        bool isSorted;
        for (int i = 0; i < 6; i++)
        {
            isSorted = (players[i].turn_order != NULL);
        }
        // for (int i = 0; i <= m_NUM_PLAYERS; i++) { this->isSorted = !(players[i].turn_order == NULL); }
        return isSorted;
    }

    player_type *GetPlayerArray() { return players; }
    player_type GetPlayer(int i) { return players[i]; }
};

#endif
