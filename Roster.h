#ifndef __ROSTER_H__
#define __ROSTER_H__
#include "Player.h"

const int NUM_PLAYERS = 6;


//class that holds the player data
class Roster
{

public:
    static const int NUM_PLAYERS = 6;
    player_type players[NUM_PLAYERS];
    
    //constructor
    Roster()
    {   
        this->players[0].name = "Von Stark";
        this->players[0].player_name = "Vincent";
        this->players[0].turn_order = NULL;
        

        this->players[1].name = "Greeb";
        this->players[1].player_name = "Emily";
        this->players[1].turn_order = NULL;

        this->players[2].name = "Arkris";
        this->players[2].player_name = "Micheal";
        this->players[2].turn_order = NULL;

        this->players[3].name = "Wreeii";
        this->players[3].player_name = "Camden";
        this->players[3].turn_order = NULL;

        this->players[4].name = "Grease";
        this->players[4].player_name = "Parker";
        this->players[4].turn_order = NULL;
        /*
    players[6].name = "Yuri";
    players[6].player_name = "Sean";
    */
        this->players[5].name = "Ali";
        this->players[5].player_name = "Liam";
        this->players[5].turn_order = NULL;

        this->players[6].name = "ENEMY";
        this->players[6].player_name = "DM";
        this->players[6].turn_order = NULL;
    }
    
};

#endif