#ifndef __NULLABLEINT_H__
#define __NULLABLEINT_H__

class NullableInt
{
public:
    NullableInt(const int value) { m_value = value; }
    void Set(const int value) { m_value = value; }
    int Get() const { return m_value; }
    bool operator==(const int &value) const { return m_value == value; }
    bool operator!=(const int &value) const { return m_value != value; }

private:
    int m_value = 0;
};
#endif
