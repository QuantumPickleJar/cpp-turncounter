#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;
#include <TouchScreen.h>
#include "GUIHandler.h"
// #include "NullableInt.h"
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define FORESTGREEN   0x0400
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GRAY    0xAD35
#define GRAY1    0xAD35
#define GRAY2    0x62EC
#define GRAY3    0x3186
#define GRAY4    0x6B0C
#define GRAY5    0x83D0

Adafruit_GFX_Button next_btn, reset_btn, start_btn, r0, r1, r2, r3, r4, r5, r6;
RosterManager rManager;
GUIHandler gui(rManager, next_btn, reset_btn, start_btn, r0, r1, r2, r3, r4, r5, r6);

// GUIHandler gui(next_btn, reset_btn, start_btn, r0, r1, r2, r3, r4, r5, r6);
// GUIHandler* gui = new GUIHandler(
//     Adafruit_GFX_Button next_btn,
//     Adafruit_GFX_Button reset_btn,
//     Adafruit_GFX_Button start_btn,
//     Adafruit_GFX_Button r0,
//     Adafruit_GFX_Button r1,
//     Adafruit_GFX_Button r2,
//     Adafruit_GFX_Button r3,
//     Adafruit_GFX_Button r4,
//     Adafruit_GFX_Button r5,
//     Adafruit_GFX_Button r6);



NullableInt* selected_index;

void setup(void)
{    
    selected_index == NULL; 
    Serial.begin(9600);
    uint16_t ID = tft.readID();
    Serial.print("TFT ID = 0x");
    Serial.println(ID, HEX);
    Serial.println("Calibrate for your Touch Panel");
    if (ID == 0xD3D3) ID = 0x9486; // write-only shield
    tft.begin(ID);
    tft.setRotation(1);            //LANDSCAPE
    tft.fillScreen(BLACK);
    gui.makeButtons(); //setup the buttons
}  


/*
 * updating multiple buttons from a list
 * you can place button addresses in separate lists
 * e.g. for separate menu screens
 */

// Array of button addresses to behave like a list
Adafruit_GFX_Button *buttons[] = {&r0, &r1, &r2, &r3, &r4, &r5, &r6, &reset_btn, &next_btn, &start_btn, NULL};
// Adafruit_GFX_Button *buttons[] = {gui::r0, gui::r1, gui::r2, gui::r3, gui::r4, gui::r5, gui::r6, gui::reset_btn, gui::next_btn, gui::start_btn, NULL};



/* update the state of a button and redraw as reqd
 *
 * main program can use isPressed(), justPressed() etc
 */
bool update_button(Adafruit_GFX_Button * b, bool down)
{
    b->press(down && b->contains(gui.GetX(),gui.GetY()));
    if (b->justReleased())
        b->drawButton(false);
    if (b->justPressed())
        b->drawButton(true);
    return down;
}


/* most screens have different sets of buttons
 * life is easier if you process whole list in one go
 */
bool update_button_list(Adafruit_GFX_Button **pb)
{
    bool down = gui.Touch_getXY();
    Serial.println(down);
    for (int i = 0 ; pb[i] != NULL; i++) {
        update_button(pb[i], down);
    }
    return down;
}

// void InitPrinterAt(uint16_t x, uint16_t y, uint8_t textsize = 2 )
void initPrinterAt(uint16_t x, uint16_t y)
{
    tft.setCursor(x,y);
    tft.setTextColor(WHITE);
    tft.setTextColor(WHITE,BLACK);
    // tft.setTextSize(textsize);
    tft.setTextSize(2);
    tft.setTextWrap(true);
}



void changeLED(int index){
    
}


void loop(void)
{
    int btnRecent;
    update_button_list(buttons); 
    int localIndex;
    // Serial.println(gui.);
    
    if(reset_btn.justPressed()){
        
        tft.fillScreen(BLACK);
        gui.makeButtons();    
    }

    if(next_btn.justPressed()){
        //poll the currently selected player's turn order
        // RosterManager* rm = new RosterManager;
        if(rManager.AnyTurnSet()) return;

        int current_order = rManager.GetPlayerArray()[selected_index->Get()].turn_order->Get();
        int next_player;

        if(current_order == NUM_PLAYERS){
            //restart the cycle
            next_player = 1;
        }
        else{
            for(int i = 0; i <= NUM_PLAYERS; i++ )
            {
                //find the next player
                if(rManager.GetPlayerArray()[i].turn_order->Get() == current_order + 1) {
                    next_player = i; 
                    break;
                }
            }
        }
        Serial.println("[post loop] next player: "); Serial.print(next_player);
        gui.printPlayerColored(next_player);
        changeLED(next_player);
    } 
    if(start_btn.justPressed()){ 
        //initPrinterAt(40,50);
        gui.printNames();
    }
    if(r0.justPressed()){
        localIndex = 0;
        btnRecent = 0;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        // selected_index.value = localIndex;
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
        // RosterManager::SetOrder(localIndex);
        // RosterManager::isFirstSelected = false;
    }
    if(r1.justPressed()){
        localIndex = 1;
        btnRecent = 1;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
    if(r2.justPressed()){
        localIndex = 2;
        btnRecent = 2;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
    if(r3.justPressed()){
        localIndex = 3;
        btnRecent = 3;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
    if(r4.justPressed()){
        localIndex = 4;
        btnRecent = 4;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
    if(r5.justPressed()){
        localIndex = 5;
        btnRecent = 5;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
    if(r6.justPressed()){
        localIndex = 6;
        btnRecent = 6;
        gui.printPlayerColored(localIndex, FORESTGREEN);
        selected_index->Set(localIndex);
        rManager.assignOrder(localIndex);
        rManager.isFirstSelected = false;
    }
        
    //pass btnRecent as a param for rewriting names so we only have to print over one instead of all but one

//on enemy turn, flash all LEDs.

    //todo: add refresh button that (may be disabled until all turn order is set?) re-prints the slots by turn order
}
